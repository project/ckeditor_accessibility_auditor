CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The CKEditor Accessibility Auditor module provides a button in CKEditor which,
when clicked, will run the HTML_CodeSniffer Accessibility Auditor on the source
code of the content currently in the editor.

For more information about HTML_CodeSniffer, see:
http://squizlabs.github.io/HTML_CodeSniffer/

This module is not affiliated with the Australian CMS vendor that holds the
copyright to HTML_CodeSniffer.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ckeditor_accessibility_auditor

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/ckeditor_accessibility_auditor

 * Documentation on how to set up and use the module is available at:
   https://www.drupal.org/docs/contributed-modules/ckeditor-accessibility-auditor


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

After installation, configure the module from Administration > Configuration >
Content Authoring > Text formats and editors > {Format name} > Configure.

    1. From the 'Toolbar Configuration', drag the 'Accessibility Auditor
       (HTML_CodeSniffer)' button (stylized as a bolded checkbox) from
       'Available buttons' into 'Active toolbar'.
    2. Configure the 'CKEditor plugin settings' for 'Accessibility Auditor
       (HTML_CodeSniffer)'.
    3. Customize the HTML_CodeSniffer location. By default,
       "//squizlabs.github.io/HTML_CodeSniffer/build/" is used.
    4. Customize the level of compliance from 'Default standard'.
    5. Save configuration.


MAINTAINERS
-----------

 * Martin Anderson-Clutz - https://www.drupal.org/u/mandclu
 * Stefan Ruijsenaars - https://www.drupal.org/u/stefanr-0

Supporting organization:

 * Digital Echidna - https://www.drupal.org/digital-echidna
